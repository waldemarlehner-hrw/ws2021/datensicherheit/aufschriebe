window.MathJax = {
  tex: {
    inlineMath: [["\\(", "\\)"]],
    displayMath: [["\\[", "\\]"]],
    processEscapes: true,
    processEnvironments: true,
    packages: {
      '[+]': ['color']
    },
    macros: {
      set: ["{\\Bbb #1}",1],
      multiline: ["{\\begin{eqnarray*}#1\\end{eqnarray*}}",1]
    },
    
  },
  options: {
    ignoreHtmlClass: ".*|",
    processHtmlClass: "arithmatex"
  },
  loader: {
    load: [
      '[tex]/color'
    ]
  }
};
  
document$.subscribe(() => { 
  MathJax.typesetPromise()
})
  