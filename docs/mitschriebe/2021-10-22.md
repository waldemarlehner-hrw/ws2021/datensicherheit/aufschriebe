# 2021-10-22


!!! info "Relativ Prim"
    $$\multiline{
        \forall a,b \in \set{N},\\
        \text{wenn }ggT(a,b) := 1 \\
        \text{dann ist } a \text{ relativ prim zu } b
    }$$

!!! tip "Phi-Funktion Primzahl"
    Die phi-Funktion einer primzahl p ist immer p-1
    
    $\phi(p) = p - 1$

$$\begin{eqnarray*}
\phi(p^k) &=& p^k - p^k-1 \\
\phi(p^k) &=& p^k (1-1/p) \\
\phi(10000) &=& \phi(2^4 · 5^4) \\
&=& 10000 · (1-\frac12) * (1-\frac15) \\
&=& 10000 · \frac12 · \frac45 \\
&=& 10000 · \frac4{10} \\
&=& 1000 · 4 \\
&=& 4000
\end{eqnarray*}$$

!!! info "Phi funktion unvorhersehbar"
    Es gibt kein genaues "Muster" für die Funktion. Dies ist für den RSA-Algorithmus hilfreich.

!!! info "Lineare Abbildung"
    Damit eine Abbildung als "linear" gilt muss folgende bedingung gelten:
    $f(x * y) = x * y$

    Bspw: $f(2+4) = f(2) + f(4)$ für $f(x)=2x$

    !!! info "Affine Linear"
        "Quasi" Linear, aber nicht ganz.
        Bspw $f(x)=2x+3$

!!! tip "Matrizen"
    Matrizen sind auch lineare Abbildungen und sind somit das "Analogon" zu geraden im $R^2$

!!! info "Polyalphabetische Chiffre"
    Ein Buchstabe im Klartext wird auf mehrere Chiffretext-Buchstaben projeziert.

    Die Häufigkeitsanalyse wird dadurch erheblich erschwert.

## Vigenère Chiffre

Es gibt ein Schlüsseltext und Plaintext.

Der Schlüssel wird wiederholt wenn die Schlüssellänge kleiner ist als der Klartext.
```
lukas - Schlüssel
hallo - Klartext

l = 11      h = 7   | 18 = s
u = 20      a = 0   | 20 = u
k = 10      l = 11  | 21 = v
a = 0       l = 11  | 11 = l
s = 18      o = 14  | 32 mod 26 = 6 = g
```

Chiffretext: `SUVLG`

$E(z,i) = (z+K_(i mod k)) mod n = V_z,K_(i mod k)$