# 2021-12-01

## Einwegfunktionen und Einweg-Hash-Funktionen

!!! info "Definition"
    Funktionen welche in eine Richtung mit relativ wenig aufwand ausgeführt werden kann, jedoch deren Umkehrung gar nicht, oder nur sehr schwer, zu berechnen ist.

### Beispiele

$M^e \text{ mod } n$ ist einfach. Die Umkehrung (modulare Wurzel) ist schwierig.

Wichtige Hashfunktionen:

* MD4 (Message Digest) - 128b Lang
* MD5, Verbesserung von MD4
* SHA (Secure Hash Algorithm)
* RIPE-MD Variante von MD4, 160b langer Hash-Wert.

Problem: Wenn ein Angreifen auf den Schlüssel kommt kann das Passwort entschlüsseln werden. Zum überprüfen des Passworts kann der Hash abgeglichen werden.

Hier beachten: **Salting**


## Zero Knowledge Protokoll

!!! info "Problem"
    Einwahl über Internet. Schlüssel kann nur im Klartext übertragen werden. Das Geheimnis geht über die Schnittstelle 

### Challenge and Response

Jemand überzeugen, dass man das Passwort kennt, ohne ein Bit an Informationen für das Geheimnis preiszugeben.

```mermaid
sequenceDiagram

participant A
participant B

note over A: Gibt Benutzererkennung / ID ein
A ->>B: ID
note over B: sucht ID<br>Schlüssel k in DB
note over B: wähle Zufallszahl r
B->>A: r
note over A: Res' = f(k',r)
note over B: Res = f(k,r)
A->>B: Res'
note over B: Res =? Res'


```


