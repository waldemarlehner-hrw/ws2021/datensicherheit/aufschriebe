# ggT, Euler und Fermat

## Eulersche phi-Funktion

Die Eulersche $\varphi(n)$-Funktion wird verwendet um die Anzahl der teilerfremden Zahlen $<n$ anzugeben.

Bspw. gibt $\varphi(10)$ = 4
```
ggT(1, 10) = 1
ggT(2, 10) = 2
ggT(3, 10) = 1
ggT(4, 10) = 2
ggT(5, 10) = 5
ggT(6, 10) = 2
ggT(7, 10) = 1
ggT(8, 10) = 2
ggT(9, 10) = 1
```



Das Argument der Funktion darf dabei aufgeteilt werden, **wenn die beiden Koeffizienten teilerfremd sind**:

$\varphi(10)$ = $\varphi(5·2)$ = $\varphi(5) · \varphi(2); 5 \nmid 2$

$\varphi(5) = 4$

```
ggT(1, 5) = 1
ggT(2, 5) = 1
ggT(3, 5) = 1
ggT(4, 5) = 1
```

$\varphi(2) = 1$
```
ggT(1, 2) = 1
```

!!! warning "Falsche Anwendung"
    $\varphi(4) \neq \varphi(2) · \varphi(2) \rightarrow 2 \neq 1·1$ 

    Weil $2 \nmid 2$ nicht stimmt darf nicht aufgeteilt werden. Es müsste $\text{ggT}(2,2) \stackrel{\Large{!}}{=} 1$ gelten.
    


Die $\varphi(n)$, wobei n eine Primzahl ist, ist immer $= n - 1$

## Restgleichheit

Wenn $a\text{ mod }n = b\text{ mod }n$, dann sagt man **a ist kongruent zu b modulo n**, oder mathematisch:  
$a \equiv b \text{ mod }n$, also bspw. $16 \equiv 11 \text{ mod }5$


## Satz von Euler

Gegeben dass $\text{ggT}(a,b)=1$ so gilt $a^{\varphi(b)}\equiv 1 \text{ mod } b$

Beispiel:
$\text{ggT}(5,12)=1 \rightarrow 5^{\varphi(12)}$  
$\varphi(12) = \varphi(3) · \varphi(4) = 2·2 = 4$  
$5^{4}=625$  
$625\text{ mod }12=1$  

## Kleiner Satz von Fermat

Gegeben dass $p$ eine Primzahl ist, also $\varphi(p) = p-1$, und $\forall a \in \mathbb{Z}: p \nmid a$, so gilt:  
$a^{p-1} \equiv 1 \text{ mod }p$

### Bestimmung des multiplikativen Inversen
Fermats Satz kann verwendet werden um das Multiplikative Inverse in $\set{Z}_p$ zu bestimmen.

Da $a^{p-1} \equiv 1 \text{ mod }p$ ist lässt sich ein $a$ aus $a^{p-1}$ rausholen.  
D.h. $a·a^{p-2} = a^{p-1}$. $a^{p-2}$ definiert somit das Multiplikative Inverse

!!! tip ""
    Um also das Multiplikative Inverse von $a$ in $\set{Z}_p$ zu bestimmen wird $a^{p-2} \text{ mod } p$ berechnet.

!!!warning "TODO"
    Referenz: [https://youtu.be/o-aWqfaWVr4](https://youtu.be/o-aWqfaWVr4)  
    Aber.. ist $\forall a \in \mathbb{Z}: p \nmid a$ nicht immer gegeben?