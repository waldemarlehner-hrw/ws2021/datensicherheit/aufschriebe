# Chiffren

Eine Chiffre ist eine invertierbare (also **bijektive**) Funktion, welche verwendet wird um Zeichenketten zu ver-
und entschlüsseln.



## Monoalphabetische Chiffre

Gegeben zwei Alphabete $A = \{a_1, a_2, a_{...}\}$ und $B = \{b_1, b_2, b_{...}\}$ ($A$ und $B$ sind in der Regel dasselbe Alphabet),
so wird eine Abbildung definiert bei welcher die Elemente von $A$ nach $B$ abbilden. Das wichtige hierbei ist dass dasselbe Element aus A immer auf dasselbe Element aus B abbildet.  
Ist dies nicht gegeben, so spricht man von einer [Polyalphabetischen Chiffre](#polyalphabetische-chiffre).

Ein beispielt solch einer Abbildung:  

$$
\multiline{
A &=& \{a,b,c\} \\
B &=& \{A,B,C\} \\
\\
T&=&\begin{cases}
a \mapsto B\\
b \mapsto C\\
c \mapsto A\\
\end{cases}
}
$$

### Substitutionschiffre
"Austauschchiffre" auf Deutsch.

Die Zeichen einer Nachricht werden *vom Alphabet abhängig* vertauscht.

#### Caesar Chipher

Die Caesarschiffre ist ein Typisches Beispiel für eine Substitutionschiffre.
Man nehme sich ein Alphabet,
bspw. $A = \{a,b,c,d,…,x,y,z\}$, also $|A| = 26$ und einen Schlüssel $k$ ("Offset").  

Die Abbildung lautet:

$E(x) = x + k \text{ mod } |A|, x \in A$

```
Klartext: t  e  x  t
Index:    19 04 23 19
k = 5    + 5  5  5  5
        --------------
          24 09 28 24
mod 26
          24 09 02 24
Cyphertxt: Y  J  C  Y
```

##### Angriff

Bei Caesarcipher gibt es $|A|$ Permutationen, im Worst Case benötigt man somit $|A| - 1$ versuche

### Multiplikationschiffre

!!! warning ""
    Diese Sektion baut auf dem Konzept von Ringen und Körpern auf. Diese sollten zuerst betrachtet werden.

Ähnlich wie die Caesarchiffre, mit der Ausnahme dass die verwendete Verknüpfung × (multiplation) und nicht + (addition) ist.

$E(x) = x · k \text{ mod } |A|, x \in A, k \in \{1,…, |A|-1\}$

Beispiel mit Klartext `klartext` und Multiplikator $k = 8$

```
Text:       k   l   a   r   t   e   x   t 
Index:      10  11  00  17  19  04  23  19
k:   ×       8   8   8   8   8   8   8   8
--------------------------------------------
            80  88  00 136 152  32 184 152
mod 26:      2  10  00   6  22   6   2  22
Chiffre:     C   K   A   G   W   G   C   W
Kollisionen:!1          !2      !2  !1 
```

!!! warning "Kollisionen"
    Wie zu sehen ist kann es zu Kollisionen kommen, die Funktion ist also **nicht immer bijektiv**.

    Für $k=8$ kollidieren bspw. `k` und `x`, sowie `e` und `r`.

    Damit es keine Kollisionen gibt müssen **$|A|$ und $k$ teilerfremd** sein, d.h. dass $\text{ggT}(k,|A|) = 1$ sein muss.

    Siehe [Desmos](https://www.desmos.com/calculator/gsqgr0v1bp?lang=de) 

    Der ggT zwischen 26 und 3 ist 1. Somit sind 26 und 3 teilerfremd. Es kommt in diesem Fall zu keinen Kollisionen.

Zum Entschlüsseln wird folgende Funktion verwendet:  
$D(X) = X · k^{-1} \text{ mod } |A|, x \in A, k \in \{1,…, |A|-1\}$, d.h. es muss das Multiplikative Inverse zu k bestimmt werden.

## Polyalphabetische Chiffre


Identisch zur Monoalph. Chiffre, mit der Ausnahme dass ein Charakter auf mehrere andere Charaktere abgebildet werden kann.


Ein einfaches Beispiel ist eine Erweiterung der Caesar-Cipher, welche zu dem Offset noch den aktuellen Index nimmt:  
$E(x) = x + k + \text{ (Index von x im Text) }\text{mod } |A|, x \in A$

Beispiel mit Klartext `text` und $k=1$

```
Klartext: t  e  x  t
Index:    19 04 23 19
k = 1    + 1  1  1  1
index    + 0  1  2  3
        --------------
          20 06 26 23
mod 26
          20 06 00 23
           U  G  A  X
```
Man sieht: Die `t`s aus dem Klartext wurden auf verschiedene Cipher-Charaktere abgebildet (`U` und `X`)

### Vigenèrechiffre

Die Werte eines Klartextes und eines Schlüsseltextes werden "zusammenaddiert". Wenn der Schlüsseltext kürzer als der Klartext ist wird der Schlüsseltext wiederholt.



Klartext: `klartext` und 
Schlüsseltext: `key`

```
Text:       k   l   a   r   t   e   x   t 
Index:      10  11  00  17  19  04  23  19

Schlüssel   k   e   y   k   e   y   k   e
Index:      19  04  24  19  04  24  19  04
-----------------------------------------------
Summe:      29  15  24  36  25  28  42  23
-----------------------------------------------
mod 26:     03  15  24  10  25  02  16  23
-----------------------------------------------
Chiffre:     D   P   Y   K   Z   C   Q   X
            *1                          *1
*1: Gleicher Char im Klartext, jedoch verschieden 
    im Chiffretext
```

#### Angriffe
Das Problem beim Angriff auf die Vigenèrechiffre ist, dass die Länge des Schlüssels nicht bekannt ist.

##### Kasiski-Test
Der Kasiski-Test ist ein Versuch die Länge des Schlüsseltextes einer Vigenèrechiffre zu bestimmen. 

Der Angreifer besitzt dabei nur den Schlüsseltext.
Der Angreifer vermerkt die Positionen und die Anzahl von wiederauftretenden 
Zeichenkette einer Länge $>3$.  

Die Abstände aller wiederauftretenden Zeichenketten werden in Primfaktoren zerlegt.

Bspw. wird ein Abstand von $75$ als $5 · 5 · 3$ zerlegt.

Dies wird für alle wiederauftretenden Zeichenketten durchgeführt.

Daraufhin werden bspw. mithilfe von Brute-Force Zeichenketten der Länge des am meisten Vorkommenden Primfaktor ausgeführt.
Schlägt dies fehl, wird der nächste (also zweitmeiste) Primfaktor als Länge des Schlüssels probiert, usw.

##### Friedmann-Test

Da der Friedmann-Test komplexer ist, wurde hierfür eine eigene Seite angelegt. Siehe [Friedmann-Test](./FriedmannTest.md)

## Transpositionschiffre
"Tauschchiffre" auf Deutsch.

Die Zeichen einer Nachricht werden *untereinander* vertauscht.

Ein Beispiel: 2 Charaktere werden immer miteinander vertauscht

```
Klartext: k l a r t e x t
          <->
          L K 
              R A 
                  E T
                      T X
        -------------------
          L K R A E T T X
```

### Angriff

Gegeben ein Klartext der Länge $x$, gibt es $x!$ Permutationen

Als Beispiel: `Hi!`

$x=3$, somit $x!=3·2·1=6$

```
Hi!
H!i
iH!
i!H
!Hi
!iH
```
Mithilfe eines Brute-Force Angriffes wäre es somit möglich mit einem worst-case von $x!-1$ und einem average-case von $\frac{x!}{2}$ den Klartext zu bestimmen


## Perfektes Chiffresystem

Ein Chiffresystem gilt als **perfekt** wenn die appriori-% für ein Zeichen im Klartext = der Warscheinlichkeit im Ciphertext gilt. 
