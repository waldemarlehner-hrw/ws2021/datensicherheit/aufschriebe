# Symmetrische Verschlüsselung

Bei der symmetrischen Verschlüsselung wird derselbe Schlüssel zum Ent- und Verschlüsseln verwendet.

## One-Time Pad

Beim One-Time-Pad wird aus $\{0,1\} \mapsto \{0,1\}$ abgebildet.

Hierfür wird eine Folge an Bits (Hat Wert 0 oder 1) eines Klartextes und eines Schlüssels zusammenaddiert und modulo 2 genommen (effektiv ein **XOR**).

Das Resultat ist ein Chiffretext.

Es bildet sich daraus folgende Tabelle für jedes Bit:

```
Input:      0   0   1   1
Schlüssel:  0   1   0   1
      xor-------------------
            0   1   1   0
```
Durch das Verwenden desselben Schlüssels auf die Chiffretext bekommt man wieder den Klartext:

$$
\multiline{
    \text{Klartext } a\\
    \text{Schlüssel k}\\
    \text{Chiffretext }c &=& a \oplus k\\
    \text{Klartext }a &=& c \oplus k \\
                      &=& (a \oplus k) \oplus k \text{ | *1}\\
                      &=& a \oplus 0 \\
                      &=& a
}
$$

!!! info "*1"
    Jeder Wert mit sich selbst XOR'd ergibt 0
    ```
    k:      0   1   
    k:      0   1   
        xor--------------
            0   0   
    ```
    Jeder Input mit 0 XOR'd bleibt unverändert. (Analog zu XOR als Addition in $\set{Z}_2$)

!!! tip "Perfekt"
    Das One-Time-Pad gilt als [perfekt](./Chiffre.md#perfektes-chiffresystem).