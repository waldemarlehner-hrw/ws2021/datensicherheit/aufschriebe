# Ringe und Körper

## Ring


Es müssen folgendende Bedingungen für eine Verknüpfung zweier
Elemente erfüllt werden damit eine Struktur ein Ring ist.


!!! info "Abgeschlossenheit"
    Das durch die Verknüpfung erzeugte Element darf nicht aus der Menge der Struktur herausspringen.

!!! info "Neutrales Element"
    Es muss ein Element geben, bei welchem eine Verknüpfung zum selben Element führt.
    
    Bei Addition ist dies `0`, bei Multiplikation `1`

!!! info "Inverses Element"
    Es muss ein Element geben, bei welchem eine Verknüpfung zum neutralen Element führt.

!!! info "Distributivgesetzt"
    $\forall a,b,c \in M$ gilt:  
    $a·(b+c) = a·b + a·c$ sowie $(a+b)·c = a·c + b·c$

### Beispiel

**($Z_n$, +, ·)** ist ein kommutativer Ring.

!!! tip ""
    Der Ring ist kommutativ, da die Reihenfolge der Elemente in der Verknüpfung irrelevant ist.  
    $a·b = b·a$ und $a+b = b+a$.


Beispiel: $n=6$

| $Z_6$, × |  0  |  1  |  2  |  3  |  4  |  5  |
|:---------|-----|-----|-----|-----|-----|-----|
| **0**    |  0  |  0  |  0  |  0  |  0  |  0  |      
| **1**    |  0  |  <span style="color:red">1</span>  |  2  |  3  |  4  |  5  |      
| **2**    |  0  |  2  |  4  |  0  |  2  |  4  |      
| **3**    |  0  |  3  |  0  |  3  |  0  |  3  |      
| **4**    |  0  |  4  |  2  |  0  |  4  |  2  |      
| **5**    |  0  |  5  |  4  |  3  |  2  |  <span style="color:red">1</span>  |      

!!! tip "Nullteiler"
    Ein Ring kann nullteiler beinhalten, ein **Körper jedoch nicht**!
 
Damit ein Ring ein Körper ist muss jedes Element ein multiplikatives Inverses besitzen.  
Es gibt ein Multiplikatives Inverses wenn $a·b \text{ mod } n = 1 ; a,b \in (Z_n, +, ·)$.  
Im Klartext: **Wenn die Tabelle in jeder Zeile eine 1 (oben rot markiert) besitzt**. Dies ist in der oberen Tabelle nicht gegeben.

## Körper

Bei $(Z_5, +, ·)$ handelt es sich um einen Körper da jedes Element ein multiplikatives Inverses besitzt:

| $Z_5$, × |    1|  2|  3|  4|
|:---------|-----|---|---|---|
| **1**    |  <span style="color:red">1</span>  | 2 | 3 | 4 |
| **2**    |  2  | 4 | <span style="color:red">1</span> | 3 |
| **3**    |  3  | <span style="color:red">1</span> | 4 | 2 |
| **4**    |  4  | 3 | 2 | <span style="color:red">1</span> |

(die Null-Spalte wurde weggelassen.)