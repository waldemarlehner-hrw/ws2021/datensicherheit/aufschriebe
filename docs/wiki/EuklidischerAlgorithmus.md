
# Euklidischer Algorithmus

## "Normaler" Euklidischer Algorithmus

Wird verwendet um $\text{ggT}(a,b); a,b \in \mathbb{Z}$ zu bestimmen.


!!! example "Beispiel: $\text{ggT}(160,9)$"
    $$
    \multiline{
    &&    && && \text{Rest}\\
    \text{(1.) }&&\underline{160} &=& 17 · \underline{9} &+& 7\\
    \text{(2.) }&&\underline{9}   &=& 1·\underline{7}  &+& 2 \\
    \text{(3.) }&&\underline{7}    &=& 3·\underline{2}    &+&{\color{green} 1}\\
    \text{(4.) }&&\underline{2}     &=& 2·\underline{1}    &+&0\\
    }
    $$

    Der ggT ist hier somit $1$. (Der letzte Rest bevor 0 der Rest ist wird betrachtet)

## Erweiterter Euklidischer Algorithmus

!!! tip ""
    [Referenz](https://www.youtube.com/watch?v=QORmBQo8-j0), sowie [Youtube Durchführung](https://www.youtube.com/watch?v=7t2czgQTazM)

Beim Erweiterten Eukl. Algo. wird von der zweitletzten Zeile stets nach dem Rest ausgelöst und die darüberliegende Zeile eingesetzt.

!!! tip "Ringe"
    Wird für Ringe verwendet um das Multiplikative Inverse zu bestimmen. 

    Die Anwendung des erw. Eukl. Alg. auf bspw. $\text{ggT}(a,b)$ kann verwendet werden um das Multiplikative Inverse von $b$, also $b^{-1}$ in $\set{Z}_{a}$ zu bestimmen. Die Lösung hierfür ist der Faktor vor $b$ am Ende des Durchlaufs.

    Das darunterliegende Beispiel kann als Suche nach $9^{-1}$ in $\set{Z}_{160}$ verstanden werden.
    

!!! example "Weiterführung des oberen Beispiels"
    Zuallererst werden die Gleichungen von oben **nach dem Rest** umgestellt:

    $$
    \multiline{
        (1.): 7 &=& 1·\underline{160}-17·\underline{9}\\
        (2.): 2 &=& 1·\underline{9}-  1 ·\underline{7}\\
        (3.): 1 &=& 1·\underline{7}-3·\underline{2}
    }
    $$

    Danach wird (2.) in (3.) eingesetzt und aufgelöst.  
    **Die unterstrichenen Werte dürfen dabei nicht ausmultipliziert werden**

    $$
    \multiline{
        1 &=& 1·\underline{7} - 3· [1·\underline{9}-  1 ·\underline{7}] \\
          &=& 1·\underline{7} - 3·\underline{9} + 3·\underline{7} \\
          &=& 4·\underline{7} - 3·\underline{9} 
    }
    $$

    Nun wird 1. eingesetzt

    $$
    \multiline{
       1 &=& 4·\underline{7} - 3·\underline{9}\\
         &=& 4·[ 1·\underline{160}-17·\underline{9}] - 3·\underline{9} \\
         &=& 4·\underline{160} - 68·\underline{9} - 3·\underline{9} \\
         &=& 4·\underline{160} { \color{green} \textbf{-71}}·\underline{9}
    }
    $$

    Das Multiplikative Inverse von 9 in $\set{Z}_{160}$ ist **-71**.  
    Da wird uns in einem Modulo-Ring befinden kann 160 zum Resultat hinzuaddiert werden. Das Multiplikative Inverse ist somit 
    $-71+160=89$

    Probe: $9·89=801$, $801 \text{ mod }160 = 1$