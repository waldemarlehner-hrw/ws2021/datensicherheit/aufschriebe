# Friedmann-Test

Der Friedmann-Test versucht die Schlüssellänge einer Vigenèrechiffre über den **Koinzidenzindex** zu bestimmen.

!!! info "Paare"
    Der Friedmann-Test baut auf dem Konzept von Zeichenpaaren auf. 

    Die Anzahl der möglichen Paare mit $m$ Teilnehmens lässt sich durch $\frac{m·(m-1)}{2}$ defineren.

    $m·(m-1)$ leitet sich davon ab dass jedes Element mit allen anderen Elementen **außer Sich** (vgl. $-1$) ein Paar bilden.  
    Das Teilen durch 2 ergibt sich daraus dass $AB$ dasselbe Paar wie $BA$ ist. Und da dies für alle ELemente gilt entfällt die Hälfte der "Paare".

    Beispiel mit 4 Elementen: $\frac{4·(4-1)}2$, also $\frac{12}2$, also $6$.

    ![](./img/2022-02-01-02-56-50.png)


!!! info "$m_i$"
    $m_i$ definiert die Anzahl des Buchstaben an i-ten Index in einem Text. So würde bspw. $m_1$ die Anzahl der `a`s, $m_2$ die `b`s usw. darstellen.

    D.h. die Anzahl der Paare von `a<->a` beträgt $\frac{m_1·(m_1-1)}{2}$, Paare von `a<->b` $\frac{m_1·(m_2-1)}{2}$

    Alle gleichen Paare (also a-a, b-b,...c-c) sind somit:

    $$
    \multiline{
    n := |M| = 26 \\
    \sum_{i=1}^{n}{\frac{m_i·(m_i -1)}{2}}
    }
    $$

!!! cite "Koinzidenzindex"
    Der Koinzidenzindex $I$ gibt an, wie Warscheinlich es ist, dass wenn man zufällig zwei
    Charaktere aus einer Zeichenkette vergleicht, derselbe Charakter gefunden wird.

    Dieser Koinzidenzindex hängt von der jeweiligen Sprache ab. Der deutsche Koinzidenzindex ist übrigens $I_{deutsch}=0,0762$

    Besteht eine Zeichenkette nur aus demselben Zeichen ist $I=1$. Ist die Zeichenkette rein zufällig, so ist $I=\frac{1}{|A|}$

    Der Koinzidenzindex berechnet sich wie gefolgt:

    Die Formel lautet

    $$
    \multiline{
        \Large
        I&=&\frac{\text{Anzahl aller gleich-Paare (aa,bb,etc)}}{\text{Anzahl aller Paare}}\\
        I&=&\frac{\sum_{i=1}^{n}{\frac{m_i·(m_i-1)}{2}}}{\frac{m·(m-1)}{2}} &\text{| Kürzen der 2}\\
        I&=&\frac{\sum_{i=1}^{n}{m_i·(m_i-1)}}{m·(m-1)}
    }
    $$

!!! warning "TODO"
    Nochmal anschauen und fertigstellen