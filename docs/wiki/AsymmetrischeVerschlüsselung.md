# Asymmetrische Verschlüsselung

Im Gegenteil zum [symmetrischen Verschlüsselung](./SymmetrischeVerschlüsselung.md) gibt es bei der asymmetrischen Verschlüsselung zwei Schlüssel – Einen zum Verschlüsseln, und einen zum Entschlüsseln.

## RSA

### Bilden der Schlüssel

Der RSA-Algorithmus basiert auf der Tatsache dass es schwer ist die beiden Primfaktoren einer sehr großen Zahl zu bestimmen. 

Man wähle zwei Primzahlen, $p_1$ und $p_2$. 

Bspw. $p_1=41$ und $p_2=79$.

Man bildet $n = p_1 · p_2 = 41 · 79 = 3239$

Als nächstes bestimmt man die [$\varphi(n)$](./Euler.md#eulersche-phi-funktion) $= \varphi(p_1) · \varphi(p_2) = (p_1-1) · (p_2-1) = 40 · 78 = 3120$

Man wählt nun eine Zahl welche Teilerfremd zu $\varphi(n)$ ist, also der $\text{ggT}(\varphi(n),x) = 1$ ist.

Hier kann bspw. $2851$ genommen werden da $\text{ggT}(3120, 2851) = 1$

Der gewählte Wert ($2851$) wird als **public Key** ($e$ für Encrypt) verwendet.

$e=2851$

Um den **private Key** ($d$) zu bestimmen muss das multiplikative Inverse von $e$ in $\set{Z}_n$ bestimmt werden.

In diesem Fall ist $e^{-1} = d = 192$

### Verschlüsselung

Gegeben einer Nachricht $m$, kodiert als Zahl $m ;0 < m < n$, so wird die Nachricht mit $e$ exponenziert.

Also $m^e \text{ mod } n$, bspw $1234^{2851} \text{ mod } 3239$

#### Square and Multiply

Da der Rechner $1234^{2851} \text{ mod } 3239$ aufgrund der größe nicht berechnen kann wird eine modifizierung des Square and Multiply Algortihmus verwendet.

##### Normaler Square and Multiply

Gegeben eine Aufgabe wie $5^34$, so wird die $34$ zuerst binär aufgeschrieben.
```
34 =    1   0   0   0   1   0   = 0b100010
        ×32 ×16 ×8  ×4  ×2  ×1
```
Für jede $1$ wird nun ein $QM$, für jede $0$ ein $Q$ eingesetzt
```
0b 1  0 0 0 1  0
   QM Q Q Q QM Q
```
Die Zahl ($1$) wird nun bei jedem $Q$ quadriert, bei jedem $M$ mit der Basis ($5$) multipliziert.
also
```
Q:             1²   =            1
M:             1·5  =            5
Q:             5²   =           25
Q:            25²   =          625
Q:           625²   =       390625
Q:        390625²   = 152587890625
M:  152587890625·5  = 762939453125
Q:  762939453125²   = 5.82076609135e+23
```
                     
##### Normaler Square and Multiply mit Modulo

Der Regelsatz wird dadurch erweitert dass bei jeder Multiplikation das Resultat $\text{mod }n$ genommen wird.  
**Man bedenke dass eine Quadrierung auch eine Multiplikation ist**

Um das Beispiel von oben aufzugreifen:

$1234^{2851} \text{ mod } 3239$

$2851$ ist `101100100011` binär. Daraus bildet sich `QMQQMQMQQQMQQQQMQM`.

```
Q:              1²      = 1
M:              1·2851  = 2851

    etc...
```