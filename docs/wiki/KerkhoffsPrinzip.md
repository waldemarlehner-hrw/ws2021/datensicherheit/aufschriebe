# Kerkhoffs' Prinzip

Kerkhoffs' Prinzip besagt dass die stärke eines kryptographischen Verfahrens **nicht** auf der Geheimhaltung der Implementierungsdetails berufen soll, sondern auf der Geheimhaltung des Schlüssels.

