# Allgemeines zur Kryptographie

## Datensicherung

engl. *data safety*

Physikalische Maßnahmen welche Sicherstellen das es zu keinem Datenverlust kommt – bspw. Backups

## Datensicherheit

engl. *data security*

Mathematische und logische Maßnahmen zur Sicherstellung dass eine unauthorisierte Person keinen Zugriff zu Daten erlangt. – bspw. Verschlüsselung

## Datenschutz

engl. *data privacy* 

Der Gedanke dass **personenbezogene Daten** (Achtung: nicht persönliche Daten) besonderst geschützt werden müssen. Es gelten besondere Bedingungen für die Verwendung (**Erfassung, Verarbeitung, Speicherung, Weitergabe**) personenbezogener Daten.


## Primäre Ziele des Kryptographie

### Authentizität
Der Versender ist tatsächlich die Person, welche die Nachricht verfasst hat.

### Integrität
Die Nachricht wurde nicht manipuliert oder beschädigt

### Vertraulichkeit
Der Inhalt der Nachricht kann nur vom Empfänger und Versender gelesen werden

### Verbindlichkeit
Wie eine Unterschrift, der Versender der Nachricht kann später nicht behaupten nie unterschrieben zu haben.